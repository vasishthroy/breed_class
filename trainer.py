import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F
from torch.optim import lr_scheduler
from torchvision import models



device = torch.device('cpu')
model = models.resnet18(pretrained=True)
num_ftrs = model.fc.in_features
model.fc = nn.Linear(num_ftrs, 38)
model.to(device)
criterion = nn.CrossEntropyLoss()
optmizer = optim.SGD(model.parameters(), lr=0.001)
step_lr_scheduler = lr_scheduler.StepLR(optmizer,step_size=7, gamma=0.1)

model = train_model(model, criterion, optmizer, step_lr_scheduler, num_epochs=10)