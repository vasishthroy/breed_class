# import torchvision
# from torchvision import datasets
# import torchvision.transforms as transforms
import os, json

# Paths
data_dir = os.path.join('data', "content")
train_dir = os.path.join(data_dir, "train")
val_dir = os.path.join(data_dir, "val")

# name = os.listdir(train_dir)
# breed = {"breed": name}

# with open(os.path.join("model", "class.json"), "w") as jf:
#     json.dump(breed, jf, indent=4)

# Data Loader
dat_transf = {
    'train': transforms.Compose([
        transforms.RandomResizedCrop(224),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
    'val': transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
}

# Train and Test
trte = ("train", "val")
image_datasets = {"train": datasets.ImageFolder(train_dir, 
                                                dat_transf["train"]),
                   "val": datasets.ImageFolder(train_dir, 
                                                dat_transf["val"]}

dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=4,
                                             shuffle=True, num_workers=4)
                for x in trte}

dataset_sizes = {x: len(image_datasets[x]) for x in trte}
class_names = image_datasets['train'].classes