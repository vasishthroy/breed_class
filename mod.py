# Importing Modules
import os
import time
import copy

from PIL import Image

import numpy as np
import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F
from torch.optim import lr_scheduler
from torchvision import models
import torchvision.transforms as transforms
import json



class_path = os.path.join("model", "class.json")


class CDBreed:
    
    def __init__(self, phase, device=torch.device("cuda")):
        self.cls_dict = {}
        self.phase = phase
        self.device = device

        self.model = models.resnet18(pretrained=True)
        num_ftrs = self.model.fc.in_features
        self.model.fc = nn.Linear(num_ftrs, 38)

        self.criterion = nn.CrossEntropyLoss()
        self.optmizer = optim.SGD(self.model.parameters(), lr=0.001)
        self.sheduler = lr_scheduler.StepLR(self.optmizer, 
                                                step_size=7, 
                                                gamma=0.1)

    def load(self, path):
        self.model.load_state_dict(torch.load(path))

    def predict(self, img_path, class_path=class_path):
        with open(class_path, "r") as jf:
            cls_dict = json.load(jf)

        image = Image.open(img_path)

        test_transforms = transforms.Compose([
                                transforms.Resize(256),
                                transforms.CenterCrop(224),
                                transforms.ToTensor(),
                                transforms.Normalize([0.485, 0.456, 0.406], 
                                                        [0.229, 0.224, 0.225])])
        image = test_transforms(image)
        self.model.eval()   # Set model to evaluate mode

        image = image.resize((224,224))
        image = np.array(image.getdata()).reshape(*image.size,3)
        image = image/255
        image = image.transpose(2,0,1)
        image = image[None,:,:,:]
        img = torch.from_numpy(image)
        #img= img.cuda() # Disabled since the model don't use gpu (my 1050ti 4gb can't handle it)
        img = img.float()
        # pass to CNN to recognize it
        outputs = self.model(img)
        _, predicted = torch.max(outputs, 1)
        return self.cls_dict["breed"][predicted]

        

    def train(self, num_epochs):
        since = time.time()

        best_model_wts = copy.deepcopy(self.model.state_dict())
        best_acc = 0.0

        for epoch in range(num_epochs):
            print(f'Epoch {epoch}/{num_epochs - 1}')
            print('-' * 10)


            self.model.train()  # Set model to training mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for inputs, labels in dataloaders[phase]:
                inputs = inputs.to(self.device)
                labels = labels.to(self.device)

                # zero the parameter gradients
                self.optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = self.model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = self.criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    loss.backward()
                    self.optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

            self.scheduler.step()

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            print('Loss: {:.4f} Acc: {:.4f}'.format(epoch_loss, epoch_acc))

            # deep copy the model
            # if phase == 'val' and epoch_acc > best_acc:
            #     best_acc = epoch_acc
            #     best_model_wts = copy.deepcopy(self.model.state_dict())
            print()


        time_elapsed = time.time() - since
        print('Training complete in {:.0f}m {:.0f}s'.format(
            time_elapsed // 60, time_elapsed % 60))
        print('Best val Acc: {:4f}'.format(best_acc))

        # load best model weights
        self.model.load_state_dict(best_model_wts)
        # return self.model


def main():
    # Paths
    data_dir = 'data'
    train_dir = os.path.join(data_dir, "train")
    val_dir = os.path.join(data_dir, "val")


if __name__ == '__main__':
    main()